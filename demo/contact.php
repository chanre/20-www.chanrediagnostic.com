<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>
        Chanre Diagnostic Laboratory </title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/formValidation.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css">

  <link rel="icon" type="image/png" href="images/nopics.jpg" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="department-page">
    <div class="loader">
        <div class="preview" style="background: rgba(255, 255,255,0.7) url('images/oval.svg') center center no-repeat; background-size:125px;"></div>
    </div>
    <?php include ('layout/header.php'); ?>
    <div class="content">
        <!--banner starts -->
        <section id="sub-page-banner" class="sub-page-banner">
            <div class="layer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="subpage-banner-text">
                                <h4 class="subpage-head">CONTACT US</h4>
                                <h5><a href="index.html">HOME</a> <i class="icofont icofont-double-right"></i> <a href="contact.html">CONTACT</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--banner ends -->
        <main class="main">
            <!-- Map starts -->
        
            <!-- Map ends -->
            <!-- Contact Form starts -->
            <section id="contact-page-form" class="contact-page-form">
                <div class="container">
                    <div class="row">
                        <div class="appoinment-form pulse wow" data-wow-duration="1s">
                            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                                <form id="contact-form-fields" action="https://premid.svarun.in/contact-mail-part.html" method="post" class="contact-form-fields form-horizontal" data-fv-framework="bootstrap" data-fv-message="This value is not valid" data-fv-feedbackicons-valid="glyphicon glyphicon-ok" data-fv-feedbackicons-invalid="glyphicon glyphicon-remove" data-fv-feedbackicons-validating="glyphicon glyphicon-refresh">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 no-l-padding">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input class="form-control" placeholder="Name:" id="name" name="name" type="text" data-fv-notempty="true" data-fv-notempty-message="The name is required" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 responsive-padding">
                                                <input class="form-control" placeholder="E-mail:" id="email" name="email" type="email" data-fv-notempty="true" data-fv-notempty-message="The email address is required" data-fv-emailaddress="true" data-fv-emailaddress-message="The input is not a valid email address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-l-padding">
                                        <div class="form-group textarea-formgroup">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control message-area" placeholder="Your message..." cols="50" rows="4" id="message" name="message" data-fv-notempty="true" data-fv-stringlength="true" data-fv-stringlength-max="200" data-fv-stringlength-message="The message must be less than 200 characters" data-fv-notempty-message="The message is required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <button type="submit" class="btn btn-default" name="signup">Send Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="address-details">
                                    <ul>
                                        <li><i class="icofont icofont-ui-dial-phone"></i> <span>+91 80 40810611 </span></li>
                                        <li><i class="icofont icofont-ui-message"></i> <span><a href="mailto:zerocaps@gmail.com" > chanrericr@gmail.com</a></span></li>
                                        <li><i class="icofont icofont-location-pin"></i> <span># 121/1, 3rd Main Road, Between 10th & 11th Cross,
Margosa Road, Malleshwaram,
Bangalore - 560003</span></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Contact Form ends -->





        </main>
    </div>
    <?php include('layout/footer.php') ?>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/formValidation.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="js/premedi_custom.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>

</html>
