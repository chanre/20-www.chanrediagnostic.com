    <section id="footer" class="footer">
        <div class="footer-widget">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer-responsive-media">
                        <h4 class="icon-head">About Us</h4>
                        <div class="footer-about">
                            <p>ChanRe Diagnostic Laboratory is a NABL-accredited clinical and diagnostic laboratory maintaining the highest ethical standards, quality, efficiency, and commitment to meet the customer's expectations.</p>
                            <ul>
                                <li><i class="icofont icofont-location-pin"></i> # 121/1, 3rd Main Road, Between 10th & 11th Cross,
Margosa Road, Malleshwaram,
Bangalore - 560003</li>
                                <li><i class="icofont icofont-ui-message"></i> <a href="mailto:zerocaps@gmail.com">chanrericr@gmail.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer-responsive-media">
                        <h4 class="icon-head">Important Links</h4>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-6">
                                <div class="footer-nav-list">
                                    <ul>
                                        <li><a href="https://www.chanrericr.com/appointment1.php"><i class="icofont icofont-double-right"></i> Appoinment</a></li>
                                        <li><a href="#"><i class="icofont icofont-double-right"></i> Our Specialists</a></li>
                                        <li><a href="contact.php"><i class="icofont icofont-double-right"></i> Why Choose Us</a></li>
                                        <li><a href="services.php"><i class="icofont icofont-double-right"></i> Our Services</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-6">
                                <div class="footer-nav-list">
                                    <ul>
                                      
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer-responsive-media">
                        <h4 class="icon-head">Location</h4>
                        <div class="insta-widget">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.538706334997!2d77.56729011413553!3d13.001329617744613!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae162ec2d6d6b1%3A0xd6455e25f0c44f98!2sChanRe%20Diagnostic%20Laboratory!5e0!3m2!1sen!2sin!4v1588396411053!5m2!1sen!2sin"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <p class="copyrights">Copyright <i class="icofont icofont-copyright"></i> 2017. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>