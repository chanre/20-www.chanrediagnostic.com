 <header class="header">
        <div class="top-container">
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-2 col-lg-3">
                           <img src="images/company_logo.png" height="50" width="200">
                        </div>
                        <div class="hidden-xs col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="call-now text-center">
                                <p><span>Call Now:</span><a href="tel:+8800123456789"> +91 080 40810611</a></p>
                            </div>
                        </div>
                        <div class="hidden-xs col-xs-12 hidden-sm col-sm-3 col-md-4 col-lg-3">
                            <div class="email text-center">
                                <p><span>E-mail:</span><a href="mailto:zerocaps@gmail.com"> chanrericr@gmail.com</a></p>
                            </div>
                        </div>
                        <div class="hidden-xs col-xs-12 col-sm-3 col-md-2 col-lg-3">
                            <div class="book-appoinment text-right">
                                <a href="http://www.chanrelabresults.com/" class="btn btn-default">Lab Report</a>
								
                            </div>
                        </div>
                        <!-- Modal -->
                       
                    </div>
                </div>
            </div>
            <div id="sticky_navigation_wrapper" class="navbar-primary">
                <div id="sticky_navigation" class="sticky_navigation">
                    <div class="top-search" style="display:none;">
                        <div class="container">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-search-alt-1"></i></span>
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-addon close-search"><i class="icofont icofont-ui-close"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 site-logo-part">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                    <div class="logo-image">
                                        <a href="#" class="brand js-target-scroll">
                                            <span class="normal-span">CDL</span>
                                        </a>
                                    </div>
                                    <div class="search-part">
                                        <div class="menu-search hidden-lg hidden-md">
                                            <a href="#"><i class="icofont icofont-search-alt-1"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 menu-part">
                                <div class="collapse navbar-collapse" id="navbar-collapse">
                                    <ul class="nav navbar-nav underline">
                                        <li class=" active "> <a href="index.html" title="HOME" id="home">HOME</a></li>
                                        <li class=""> <a href="about.php" title="ABOUT" id="about-page">ABOUT</a></li>
                                        <li class=""> <a href="packages.php" title="DEPARTMENTS" id="departments">PACKAGES</a></li>
                                        <li class=" dropdown "> <a  href="services.php" >SERVICES</a></li>
										<li class=" dropdown "> <a  href="news.php" >NEWS</a></li>
                                        
                                        <li class=""> <a href="contact.php" title="CONTACT" id="contact">CONTACT</a></li>
                                    </ul>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>