<!DOCTYPE html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>Chanre Diagnostic Laboratory</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/formValidation.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css">

   <link rel="icon" type="image/png" href="images/nopics.jpg" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="sevices-page">
    <div class="loader">
    <div class="preview" style="background: rgba(255, 255,255,0.7) url('images/oval.svg') center center no-repeat; background-size:125px;"></div>
</div>
<?php include ('layout/header.php'); ?>
    <div class="content">    <!--banner starts -->
    <section id="sub-page-banner" class="sub-page-banner">
    <div class="layer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="subpage-banner-text">
    <h4 class="subpage-head">OUR SERVICES</h4>
    <h5><a href="index.html">HOME</a> <i class="icofont icofont-double-right"></i> <a href="#">PAGE</a></h5>
    </div>
</div>
</div>
</div>
</div>
</section>
    <!--banner ends -->
    <main class="main">
    <!-- our Services starts -->
        

    <section id="our-services" class="our-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <div class="top-text">
                        <h2>
                            Our Services                      </h2>
                        <p></p>
                        <div class="divider">
                            <span><i class="icofont icofont-bed-patient"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
               <p>
                   <h2 align="center">Clinical and Diagnostic Services</h2> <br>
                   Chanre Diagnostic Laboratory is one of the best diagnostic centers providing the widest test portfolios especially in the field of immunodiagnostics. It is attuned to the latest trends and technologies, and uses state-of-the-art instruments to deliver test results to patient in a reliable, timely, and cost effective way. The support, hard work, and dedication of our highly efficient team comprising of pathologists, microbiologists, biotechnologists, biochemists, and molecular biologists helped us to set newer standards in the diagnostic field.
               </p>
               <p><br>
                   <h2 align="center">Routine Diagnostic Assays</h2><br>
                   Realizing the criticality of pathology testing to medical diagnosis and treatment, CDL is committed to bring quality testing, though innovation and continued improvisation. The following are the routine diagnostic assays conducted in the laboratory:
               </p>
			      <p> <br>
                    <h3>Laboratory Services</h3>
                    <div class="block">
Clinical Biochemistry<br>
Pathology &amp; Cytopathology<br>
Serology &amp; Microbiology<br>
Hematology<br>
Molecular Biology<br>
Histopathology<br>
Drug of Abuse<br>
Hormonal Assays<br>
Tumor Markers<br>
Coagulation Assays<br>
Therapeutic Drug Monitoring<br>
Immunohistochemistry
        </div>
                </p> 
               <p><br>
                   <h3>
                       Radiology Service
                   </h3>
                   Ultrasound Scan with 3-D and 4-D imaging<br>
Digital X-Ray<br>
Digital Mammography<br>
Dexa Bone Densitometry (BDM)
               </p>
               <p><br>
                   <h3>Cardiology</h3>
                   Electrocardiogram (ECG)<br>
Tread mill Test (TMT)<br>
Echocardioram (ECHO)<br>
Non-invasive electro-cardiophysiology
Doppler
               </p><br>
              
                <p >
                    <h2 align="center">Specialized Assays</h2>
                    In addition to the routine assays, the lab is conducting specialized immune and molecular diagnostic assays. The assays are listed below:
                </p>    <br>        
                    <p>
                      <h3>ImmunoDiagnostic Assays</h3>
                      <div class="block">
ANA (by immunoflourescence)<br>
Anti ds DNA (ELISA)<br>
ANA profile (immunoblot assay)<br>
ANCA (by immunoflourescence)<br>
ANCA/ Vasculitis Profile<br>
Anti GBM antibody<br>
ANTICCP<br>
HsCRP<br>
Anti-mitochondrial Antibody<br>
Myositis profile<br>
Flow cytometry
</div>
                    </p><br>
                    <p>
                        <h3>Molecular Diagnostic assays</h3>
                        <div class="block">
PCR for MTB<br>
HIV viral load (Quantitative PCR)<br>
Qualitative PCR for HIV<br>
HCV viral load<br>
HBV viral load<br>
        </div>
                    </p>
            </div>
        </div>
    </section>
        <!-- our Services ends -->
    <!-- CTA starts -->
    <section id="cta" class="cta">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="cta-text">
                    <h3 class="fadeInDown wow" data-wow-duration="1s">WE CARE ABOUT YOU AND YOUR FAMILY</h3>
                    <p class="fadeInDown wow" data-wow-duration="1s">lorem ipsum dolor sit amet consectetur adipiscing elit pretium volutpat dui nibh porttitor montes interdum tempor</p>
                    <a href="#" class="btn btn-primary fadeInUp wow" data-wow-duration="1s">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
</section>    <!-- CTA ends -->
    <!-- Clients Logo starts -->
 
    <!-- Clients Logo ends -->
    





    </main>
    </div>    
    <?php include('layout/footer.php') ?>
    <script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/formValidation.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/premedi_custom.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>
</html>