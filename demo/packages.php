<!DOCTYPE html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>
       Chanre Diagonostic Laboratory  </title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/formValidation.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css">
   <link rel="icon" type="image/png" href="images/nopics.jpg" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
	<style>
	#price{
		font-weight:bold;
	}
	</style>
</head>

<body class="department-page">
    <div class="loader">
    <div class="preview" style="background: rgba(255, 255,255,0.7) url('images/oval.svg') center center no-repeat; background-size:125px;"></div>
</div>
<?php include ('layout/header.php'); ?>
    <div class="content">    <!--banner starts -->
    <section id="sub-page-banner" class="sub-page-banner">
    <div class="layer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="subpage-banner-text">
    <h4 class="subpage-head">OUR Packages</h4>
    <h5><a href="index.html">HOME</a> <i class="icofont icofont-double-right"></i> <a href="packages.php">Packages</a></h5>
    </div>
</div>
</div>
</div>
</div>
</section>
    <!--banner ends -->
    <main class="main">    <!-- our department starts -->
<br>
        <!-- our department ends -->
    <!-- FAQ Accordian starts -->
    <section id="department-accordian" class="department-accordian">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="faq-accordian">
                    <h3>Packages</h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Chanre Basic Health Check-Up</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>
                                       <ul>
                  <li>Complete Blood Count (CBC) with ESR —[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li> Serum Creatinine</li>
                  <li>Total Cholesterol</li>
                  <li> Random Blood Sugar</li>
                  <li> Urine Routine </li>
                  <li>Digital Chest X-ray </li>
                  <li>ECG  </li>
                  <span id="price">₹ 950/-</span>
               </ul>                         </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Chanre Comprehensive Heart Check</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>
                                       <ul>
                  <li> Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li>RBS </li>
                  <li> Serum Creatinine </li>
                  <li> Lipid Profile - [Total Cholesterol, Triglycerides, HDL, LDL, VLDL Cholesterol, Total Cholesterol HDL Ratio]  </li>
                  <li>Digital Chest X-ray</li>
                  <li>ECG  </li>
                  <li>Computerised Stress Test (TMT) or ECHO </li>
                  <li>Cardiologist Consultation  </li>
                  <li>Diet Counselling  </li>
                  <span id="price">₹ 2200/-</span>
               </ul>              </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">ChanRe OBESITY Screening Profile</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        <ul>
                  <li>  Height, Weight, BP, BMI </li>
                  <li>DEXA Scan / Body Fat Analysis </li>
               
                  <li>Physician Consultation </li>
                  <li>Dietician Consultation (twice)  </li>
                  <li>Physiotherapist Consultation(four times) And/OR  </li>
                  <li>Yoga therapist Consultation (four times)</li>
                  <span id="price">₹ 4800/-</span>
               </ul>                                   </p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">ChanRe OBESITY Screening Profile</a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        <ul>
                  <li>  Height, Weight, BP, BMI </li>
                  <li>DEXA Scan / Body Fat Analysis </li>
               
                  <li>Physician Consultation </li>
                  <li>Dietician Consultation (twice)  </li>
                  <li>Physiotherapist Consultation(four times) And/OR  </li>
                  <li>Yoga therapist Consultation (four times)</li>
                  <span id="price">₹ 4800/-</span>
               </ul>                                  </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">ChanRe Anaemia Screening Profile</a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        <ul>
                  <li> Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]  </li>
                  <li>Peripheral Smear</li>
                   <li> Serum Iron </li>
                  <li>  Serum Ferritin, TIBC </li>
                  <li> Reticulocyte Count   </li>
                  <li>Serum Bilirubin Total, Direct &amp; Indirect </li>
                  <li> Folic Acid  </li>
                  <li>Vitamin B12   </li>
                  <li> Stool for Occult Blood  </li>
                  <li>Physician Consultation            </li>
                  <span id="price">₹ 2800/-</span>
               </ul>                                   </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">ChanRe Bone Health Check </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                       <ul>
                  <li> Complete Blood Count (CBC) with ESR — [Haemoglobin [(HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li>Serum Alkaline Phosphate </li>
                  <li>CRP </li>
                  <li>  Rheumatoid Factor </li>
                  <li> Vitamin D Levels </li>
                  <li>Serum Calcium, Phosphorus  </li>
                  <li>Digital X-ray  </li>
                  <li>Bone Mineral Density (BMD) Scan </li>
                  <li>Rheumatologist Consultation   </li>
                  <span id="price">₹ 4000/-</span>
               </ul>                                   </p>
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">ChanRe Kidney Screening Profile</a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        <ul>
                  <li>Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li>Blood Urea</li>
                  <li>Serum Creatinine </li>
                  <li> Serum Electrolytes</li>
                  <li>HbA1c  </li>
                  <li>Urine Routine</li>
                  <li>Microalbumin Creatinine Ratio </li>
                  <li>Ultrasound KUB  </li>
                  <li>Physician Consultation </li>
                  <span id="price">₹ 2000/-</span>
               </ul>                                   </p>
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">ChanRe Well Women Health Check</a>
                                </h4>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                         <ul>
                  <li>Complete Blood Count (CBC) with ESR - [Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li> Lipid Profile - [Total Cholesterol, Triglycerides, HDL, LDL, VLDL Cholesterol, Total Cholesterol HDL Ratio] </li>
                  <li> Liver Function Test — [Total Direct &amp; Indirect Bilirubin, Total Protein, SGOT, SGPT, GGT, Albumin, Globulin, Alkaline Phosphates] </li>
                  <li> Blood Urea, Creatinine, Serum Calcium</li>
                  <li>FBS  </li>
                  <li>Thyroid - T3, T4, TSH  </li>
                  <li>Pap Smear</li>
                  <li>VDRL   </li>
                  <li>Urine Routine   </li>
                  <li>USG Abdomen &amp; Pelvis </li>
                  <li> Gynaecologist Consultation </li><span id="price">₹ 3200/-</span>
               </ul>                                </p>
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen"> ChanRe Senior Citizen's Health Check</a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                      <ul>
                  <li>Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count] </li>
                  <li>  FBS &amp; PPBS </li>
                  <li>  HbA1c  </li>
                  <li> Lipid Profile - [Total Cholesterol, Triglycerides, HDL, LDL, VLDL Cholesterol, Total Cholesterol HDL Ratio] </li>
                  <li> Liver Function Test — [Total Direct &amp; Indirect Bilirubin, Total Protein, SGOT, SGPT, GGT, Albumin, Globulin, Alkaline Phosphates] </li>
                  <li>Serum Phosphorous</li>
                  <li> Blood Urea, Creatinine</li>
                  <li>Digital Chest X-ray  </li>
                  <li>ECG   </li>
                  <li>Urine Routine  </li>
                  <li>TSH </li>
                  <li>Calcium    </li>
                  <li>Ultrasonography   </li>
                  <li>PSA (Men)/Pap Smear (Women) </li>
                  <li> Physician Consultation </li>
                  <li>  Ophthalmologist Consultation </li>
                  <li>  Complementary Breakfast </li>
                  <li>  Physiotherapist Consultation </li> <span id="price">₹ 3800/-</span>
               </ul>                                 </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">ChanRe Liver Screening Profile</a>
                                </h4>
                            </div>
                            <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                       <ul>
                  <li> Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li>Liver Function Test-[Total Direct &amp; Indirect Bilirubin, Total Protein, SGOT, SGPT, GGT, Albumin, Globulin, Alkaline Phosphates] </li>
                  <li>HbsAg </li>
                  <li> Anti-HCV</li>
                  <li>Urine Routine </li>
                  <li>Ultra Sonography </li>
                  <li>Physician Consultation  </li><span id="price">₹ 2000/-</span>
               </ul>                               </p>
                                </div>
                            </div>
                        </div>
 <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">ChanRe Executive Health Check</a>
                                </h4>
                            </div>
                            <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                         <ul>
                  <li>Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count]</li>
                  <li>Peripheral Smear</li>
                  <li>Blood Sugar Level - FBS &amp; PPBS with Urine Sugar</li>
                  <li>HbA1c </li>
                  <li>Lipid Profile - [Total Cholesterol, Triglycerides, HDL, LDL, VLDL Cholesterol, Total Cholesterol HDL Ratio]</li>
                  <li>Liver Function Test — [Total Direct &amp; Indirect Bilirubin, Total Protein, SGOT, SGPT, GGT, Albumin, Globulin, Alkaline Phosphates]</li>
                  <li> Serum Electrolytes — [Sodium, Potassium, Chloride]</li>
                  <li> Kidney Profile — [Blood Urea, Serum Creatinine, Uric Acid]</li>
                  <li> Thyroid - T3, T4, TSH</li>
                  <li> Serum Calcium, Phosphorus</li>
                  <li>Urine Examination - Routine, Microscopy</li>
                  <li>Stool Routine</li>
                  <li>VDRL </li>
                  <li>HIV, HBsAg</li>
                  <li>Pap Smear for Women / PSA for Men </li>
                  <li>Ultra Sonography </li>
                  <li>Digital Chest X-Ray</li>
                  <li>ECG </li>
                  <li>Fundoscopy </li>
                  <li>Computerised Stress Test (TMT) or ECHO</li>
                  <li>Cardiologist Consultation</li>
                  <li>Physician Consultation</li>
                  <li>Pulmonary Function Test </li>
                  <li>Gynaecologist Consultation  </li>
                  <li>Ophthalmologist Consultation </li>
                  <li> Diet Counselling</li>
                  <li>Complementary Breakfast </li><span id="price">₹ 7000/-</span>
               </ul>                                  </p>
                                </div>
                            </div>
                        </div>
                         <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen"> ChanRe Master Health Check</a>
                                </h4>
                            </div>
                            <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>
                                        <ul>
                  <li> Complete Blood Count (CBC) with ESR -[Haemoglobin (HB), Total Count (TC), Differential Count (DC), MCV, MCH, MCHC, PCV, RBC Count, Platelet Count] </li>
                  <li> Peripheral Smear   </li>
                  <li> Urine Examination - Routine, Microscopy  </li>
                  <li>Stool Examination   </li>
                  <li> Blood Sugar Level FBS &amp; PPBS'  </li>
                  <li>HbA1c  </li>
                  <li> Lipid Profile - [Total Cholesterol, Triglycerides, HDL, LDL, VLDL Cholesterol, Total Cholesterol HDL Ratio] </li>
                  <li>Liver Function Test — [Total Direct &amp; Indirect Bilirubin, Total Protein, SGOT, SGPT, GGT, Albumin, Globulin, Alkaline Phosphates]  </li>
                  <li> Kidney Profile — [Blood Urea, Serum Creatinine, Uric Acid]  </li>
                  <li>Serum Calcium  </li>
                  <li>TSH  </li>
                  <li>Serum Phosphorus   </li>
                  <li>Serum Electrolytes — [Sodium, Potassium, Chloride]   </li>
                  <li>Digital Chest X-ray  </li>
                  <li>ECG  </li>
                  <li> PSA (Men)/Pap Smear (Women)   </li>
                  <li>Computerised Stress Test (TMT) or ECHO   </li>
                  <li>Physician Consultation  </li>
                  <li>Cardiologist Consultation   </li>
                  <li>Gynaecologist Consultation </li>
                  <li> Diet Counselling  </li>
                  <li>Complementary Breakfast </li><span id="price">₹ 5000/-</span>
               </ul>                                  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <div class="row">
                    <div class="appoinment-form">
                        <h3>Sample Collection</h3>
                        <form id="appoinment-form-fields" action="https://premid.svarun.in/mail-part.html" method="post" class="appoinment-form-fields form-horizontal" data-fv-framework="bootstrap" data-fv-message="This value is not valid" data-fv-feedbackicons-valid="glyphicon glyphicon-ok" data-fv-feedbackicons-invalid="glyphicon glyphicon-remove" data-fv-feedbackicons-validating="glyphicon glyphicon-refresh">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-l-padding">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input class="form-control" placeholder="Your Name:" id="name" name="name" type="text" data-fv-notempty="true" data-fv-notempty-message="The name is required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input class="form-control" placeholder="Your Mail" id="email" name="email" type="email" data-fv-notempty="true" data-fv-notempty-message="The email address is required" data-fv-emailaddress="true" data-fv-emailaddress-message="The input is not a valid email address" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 validate-right-icon">
                                        <select class="form-control" name="doctors" id="doctors" data-fv-notempty="true" data-fv-notempty-message="This Field is required">
                                                        <option>Doctors</option>
                                                        <option>Doctors 2</option>
                                                        <option>Doctors 3</option>
                                                        <option>Doctors 4</option>
                                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-r-padding">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <input class="form-control" placeholder="Your Phone:" id="phone" name="phone" type="text" data-fv-notempty="true" data-fv-notempty-message="Phone Number is required" data-fv-integer="true" data-fv-integer-message="The input is not a valid Phone Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 validate-right-icon">
                                        <input type="text" name="dateTo" class="form-control datepicker" placeholder="MM/DD/YYYY:" id="dateTo" readonly="readonly" value="" data-fv-notempty="true" data-fv-date="true" data-fv-date-format="MM/DD/YYYY" data-fv-date-message="The value is not a valid date" />
                                        <span class="input-group-addon add-on"><i class="icofont icofont-ui-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 validate-right-icon">
                                        <select class="form-control" name="departments" id="department" data-fv-notempty="true" data-fv-notempty-message="This Field is required">
                                                        <option>department</option>
                                                        <option>department 2</option>
                                                        <option>department 3</option>
                                                        <option>department 4</option>
                                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <button type="submit" class="btn btn-default appoinment-form-submit" name="signup" data-loading-text="Processing...">Book an Appoinment</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- FAQ Accordian ends -->





    </main>
    </div>    
<?php include ('layout/footer.php'); ?>
    <script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/formValidation.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/premedi_custom.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>
</html>