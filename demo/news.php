<!DOCTYPE html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>Chanre Diagnostic Laboratory</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/formValidation.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body class="sevices-page">
    <div class="loader">
    <div class="preview" style="background: rgba(255, 255,255,0.7) url('images/oval.svg') center center no-repeat; background-size:125px;"></div>
</div>
<?php include ('layout/header.php'); ?>
    <div class="content">    <!--banner starts -->
    <section id="sub-page-banner" class="sub-page-banner">
    <div class="layer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="subpage-banner-text">
    <h4 class="subpage-head">NEWS</h4>
    <h5><a href="index.html">HOME</a> <i class="icofont icofont-double-right"></i> <a href="#">NEWS</a></h5>
    </div>
</div>
</div>
</div>
</div>
</section>
    <!--banner ends -->
    <main class="main">
    <section id="our-services" class="our-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <div class="top-text">
                        <h2>Our NEWS</h2>
                        <p></p>
                        <div class="divider">
                            <span><i class="icofont icofont-bed-patient"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            
            </div>
        </div>
    </section>
	<div class="container">
	<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-8">
	<h2>Osteoporosis</h2>
	<div style="padding: 5px;">
<p style="text-align: justify;">One in three woman and one in five men over the age of 50 will have life-threatening fractures due to osteoporosis (bone thinning). 80% of the fractures in people over 50 are caused by osteoporosis. Yet the disease remains undiagnosed and untreated. In majority of the patients there will be no symptoms and will be identified or noticed when bone breaks(fracture). In view of that, majority of patients are neither assessed nor treated. The good news is osteoporosis can be detected early, treated and fractures can be prevented.</p>
<p style="text-align: justify;">ChanRe Diagnostic Laboratory (CDL), Malleshwaram and ChanRe Rheumatology &amp; Immunology Center &amp; Research (CRICR), Rajajinagar is arranging a free <b>Osteoporosis Assessment Camp</b> from 27th October to 4th November 2018 as a continued effort in improving the awareness. The camp is offering 50% discount on DEXA scan for checking th bone mineral density (BMD) only for patients who needs to be assessed for DEXA </p>
  
    <p style="text-align: justify;">Stand up against osteoporosis. Know the risk factors and what really matters in the prevention of Osteoporosis. Take care of your bones and improve your family's bone health.</p>
    <p style="text-align: justify;">CRICR is a specialized center in Rheumatology, Autoimmune dieases and Musculo-skeletal diseases dedicated for the patient care and CDL is a fully equipped diagnostic laboratory providing the widest test portfolios which uses state of the technology.</p></div>
	</div>
	<div class="col-md-2">
	</div>
	</div>
	</div>
	
        <!-- our Services ends -->
    <!-- CTA starts -->
    <!-- CTA ends -->
    <!-- Clients Logo starts -->
 
    <!-- Clients Logo ends -->
    





    </main>
    </div>    
    <?php include('layout/footer.php') ?>
    <script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/formValidation.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/premedi_custom.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>
</html>