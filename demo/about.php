<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from premid.svarun.in/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Apr 2020 08:24:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="155 characters of message matching text with a call to action goes here">
    <meta name="author" content="">
    <title>Chanre Diagnostic Laboratory  </title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Source+Sans+Pro:300,400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/formValidation.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css">

      <link rel="icon" type="image/png" href="images/nopics.jpg" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>


<body class="about-page">
    <div class="loader">
    <div class="preview" style="background: rgba(255, 255,255,0.7) url('images/oval.svg') center center no-repeat; background-size:125px;"></div>
</div>
<?php include ('layout/header.php'); ?>
    <div class="content">   
    <section id="sub-page-banner" class="sub-page-banner">
    <div class="layer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="subpage-banner-text">
        <h4 class="subpage-head">ABOUT US</h4>
        <h5><a href="index.html">HOME</a> <i class="icofont icofont-double-right"></i> <a href="about.html">ABOUT</a></h5>
    </div>
</div>
</div>
</div>
</div>
</section>
    <!--banner ends -->
    <main class="main">        <!-- About starts -->
    <section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center fadeIn wow"  data-wow-duration="1s">
                <img src="images/appoinment1.png" alt="" class="img-responsive hidden-xs" />            </div>
            <div class="about-premedi-response">
                <div class="open-hours-details hidden-xs hidden-md hidden-lg">
                    <h4><i class="icofont icofont-wall-clock"></i> Opening Hours</h4>
                    <ul class="col-xs-12 col-sm-6 col-md-7">
                        <li><span class="pull-left">Saturday</span> <span class="pull-right">9:00 - 20:00</span></li>
                        <li><span class="pull-left">Sunday</span> <span class="pull-right">9:00 - 20:00</span></li>
                        <li><span class="pull-left">Monday - Friday</span> <span class="pull-right">9:00 - 20:00</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="about-premedi">
                    <h4 class="premedi-story">Welcome to</h4>
                    <h3 class="about-head">ChanRe Diagnostic Laboratory</h3>
                    <p>
                       <span style="color:black;font-weight:bold;font-size: 16px;">ChanRe Diagnostic Laboratory</span> is a NABL-accredited clinical and diagnostic laboratory maintaining the highest ethical standards, quality, efficiency, and commitment to meet the customer's expectations. Established in 2004 by Dr. Renuka P the laboratory has rapidly grown into one of the most technologically advanced centres and it has ventured into the foray of complete diagnostic services.
<br>We at CDL provide a wide range of specialized diagnostic assays including routine biochemistry, haematology, clinical pathology, hormonal assays, cytology and histopathology as well as specialised assay such as molecular assays and flow cytometry. We specialise in autoimmune diagnostic assays and has been the laboratory to introduce quite a few newer autoimmune tests to the city of Bangalore. We follow the highest ethical and quality standards and use the latest technology and advanced instruments to ensure accurate and precise results. Full time qualified and experienced team of pathologists and MD biochemists are available for clinician queries.
<br>Keeping in mind comfort to patients we provide facilities such home collection services and online availability of test results. The center has availability of equipment like X-ray,  ultrasound, Pulmonary Function Test (PFT),BMD scanning, Doppler besides laboratory tests. It is centrally located and easily accessible through all modes of transportation. It is recognised by various corporate, third party agencies for insurance and public sector organisation such as CGHS, Karanataka govt, Nimhans, BHEL and most recently with NAL providing our services at CGHS rates.                </p>

                    <div class="open-hours-details hidden-sm">
                        <h4><i class="icofont icofont-wall-clock"></i> Opening Hours</h4>
                        <ul class="col-xs-12 col-md-8 col-lg-7">
                            <li><span class="pull-left">Saturday</span> <span class="pull-right">9:00 - 20:00</span></li>
                            <li><span class="pull-left">Sunday</span> <span class="pull-right">9:00 - 20:00</span></li>
                            <li><span class="pull-left">Monday - Friday</span> <span class="pull-right">9:00 - 20:00</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-6">
                <p><div align="center">
                    <img src="images/renuka.jpg">
                </div>
                    <h4 align="center"> Dr. RENUKA.P</h4>
                    <h4 align="center">MBBS, DCP, DNB
(Pathologist )</h4>

Dr.Renuka.P after completing her MBBS from B.J.Medical College, Pune, started her journey towards specialization in Pathology by completing DCP in 1999 and DNB Pathology in 2001 from St. John's Medical College Bangalore. After working in St. John's medical college as a lecturer for two year, she established ChanRe Diagnostic Laboratory in 2003. Over the years she was instrumental in bring up CDL to the current status of a recognized referral medical diagnostic laboratory with all the branches of diagnosis and a unique specialty unit for Immunodiagnosis. Under her leadership with able team of Pathologist, Biochemist & Microbiologist she was able to get the NABL Accreditation to CDL in current year 2010. She has a few publications to her credit & she was instrumental in disseminating knowledge by organizing several Workshops in Immunodiagnosis & other fields of Pathology. She is the best half of Dr.Chandrashekara.S and blessed with two daughters.
                </p>
            </div>
            <div class="col-md-6">
                <p><div align="center">
                    <img src="images/chandrashekara.jpg">
                </div>
                    <h4 align="center">Dr. CHANDRASHEKARA.S</h4>
                    <h4 align="center">MBBS, MD, DM
(Clinical Immunology & Rheumatology)</h4>

Dr.Chandrashekara.S completed D.M (clinical Immunology and Rheumatology) from SGPGI, Lucknow in the year 1996. He started his career by initiating and establishing a department of Immunology at M.S Ramaiah medical college, Bangalore as Assistant Professor and Consultant Immunologist. He was elevated as Professor in June 2006 for his pioneering and active research work in the field of Immunology and Rheumatology. As part of his vision he established CRICR a specialized one-stop centre for complete care and treatment in Autoimmune disease in the year 2002. This centre ChanRe Rheumatology & immunology Centre and Research is also keen in concentrating on the application of modern biology in current medical practice. He has currently 13 international publications and 7 National publications to his feathers. He has organised various CMEs, Immunodiagnostic Workshop, Rehabilitation Programmes and Free Arthritis Camps to create awareness about the Rheumatological disease among under privileged society.
                </p>
            </div>
        </div>
        <br>
    </div>
</section>
     



        <!-- Our Specialist ends -->
        <!-- Parents Feedback starts -->
    
 
      <!-- Parents Feedback ends -->
    
    </main>
    </div>    
  <?php include ('layout/footer.php'); ?>
    <script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/formValidation.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/premedi_custom.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
</body>

<!-- Mirrored from premid.svarun.in/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Apr 2020 08:24:56 GMT -->
</html>